#include<stdio.h>
int input()
{
    int y;
    printf("Enter a year\n");
    scanf("%d",&y);
    return y;
}
void check_leap(int y)
{
    if((y%4==0)&&(y%100!=0))
        printf("%d is leap year\n",y);
    else if(y%400==0)
        printf("%d is a leap year\n",y);
    else
        printf("%d is not a leap year\n",y);
}        
int main()
{
    int y;
    y=input();
    check_leap(y);
    return 0;
}